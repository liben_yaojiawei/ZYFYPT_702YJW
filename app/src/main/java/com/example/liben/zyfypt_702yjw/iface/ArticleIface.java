package com.example.liben.zyfypt_702yjw.iface;

public interface ArticleIface {
    void getResultList(String mod,
                       int page,
                       String sessionID,
                       ArticleListener listener
    );

}
