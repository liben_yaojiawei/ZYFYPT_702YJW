package com.example.liben.zyfypt_702yjw.model;

import com.example.liben.zyfypt_702yjw.iface.AttentionListener;
import com.example.liben.zyfypt_702yjw.iface.Attentioniface;
import com.example.liben.zyfypt_702yjw.service.AttentionService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.example.liben.zyfypt_702yjw.common.Common.BASEURL;


public class AttentionModel implements Attentioniface{
    private Retrofit retrofit;
    //构造函数
    public AttentionModel(){
        //使用Retrofit----1
        retrofit=new Retrofit.Builder()
                .baseUrl(BASEURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

    }
    //关注
    @Override
    public void attetntion(String mod, int id, String sessionid, final AttentionListener listener) {
        //使用Retrofit----2
       AttentionService attentionService=retrofit.create(AttentionService.class);
        Call<String> call=attentionService.focus(mod,id,sessionid);
        //使用Retrofit----3
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful() && response!=null)
                {  if(response.body().trim().toString().equals("0"))
                    listener.onResponse("1");//关注失败
                else if(!response.body().trim().toString().contains("error"))
                    listener.onResponse("2");//关注成功
                else
                    listener.onResponse("关注："+response.body());


                }
                else   listener.onFail("attetntion onresponse fail");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                listener.onFail("关注失败："+t.toString());
            }
        });
    }
    //取消关注
    @Override
    public void unattetntion(String mod, int id, String sessionid, final AttentionListener listener) {
        //使用Retrofit----2
        AttentionService  attentionService=retrofit.create(AttentionService.class);
        Call<String> call=attentionService.unfocus(mod,id,sessionid);
        //使用Retrofit----3
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful() && response!=null)
                {  if(response.body().trim().toString().equals("0"))
                    listener.onResponse("3");//取消关注失败
                else if (response.body().trim().toString().equals("1"))
                    listener.onResponse("4");//取消关注成功
                else
                    listener.onResponse("取消关注："+response.body());

                }
                else   listener.onFail("unattention onresponse fail");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                listener.onFail("取消关注失败："+t.toString());
            }
        });


    }
    //判断是否关注
    @Override
    public void exist(String mod, int id, String sessionid, final AttentionListener listener) {
        //使用Retrofit----2
        final AttentionService  attentionService=retrofit.create(AttentionService.class);
        Call<String> call=attentionService.exists(mod,id,sessionid);
        //使用Retrofit----3
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful() && response!=null)
                {  if(response.body().trim().toString().equals("0"))//已关注
                    listener.onResponse("5");
                else if (response.body().trim().toString().equals("1"))//未关注
                    listener.onResponse("6");
                else
                    listener.onResponse("判断关注："+response.body());

                }
                else   listener.onFail("attention exist onresponse fail");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                listener.onFail("判断关注失败："+t.toString());
            }
        });


    }
}
