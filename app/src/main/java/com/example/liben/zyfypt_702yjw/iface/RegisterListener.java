package com.example.liben.zyfypt_702yjw.iface;

import com.example.liben.zyfypt_702yjw.bean.LoginBean;

public interface RegisterListener {
    //成功返回登陆信息
    void onResponse(String n);
    //失败返回错误字符串
    void onFail(String msg);
}
