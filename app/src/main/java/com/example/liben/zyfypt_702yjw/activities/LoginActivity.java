package com.example.liben.zyfypt_702yjw.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.liben.zyfypt_702yjw.R;
import com.example.liben.zyfypt_702yjw.bean.LoginBean;
import com.example.liben.zyfypt_702yjw.iface.LoginListener;
import com.example.liben.zyfypt_702yjw.model.LoginModel;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etuser, etpass;
    private String username = "", password = "", sessionID = "";
    private Switch sw;
    private SharedPreferences sp;

    private LoginListener loginListener = new LoginListener() {
        @Override
        public void onResponse(LoginBean loginBean) {

            sessionID = loginBean.getSessionid();
            System.out.println("--登陆成功"+sessionID);
            if(sessionID != null){
                saveSP();
                Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
                Toast.makeText(LoginActivity.this, "登录成功! seesionID:"+sessionID, Toast.LENGTH_SHORT).show();
                finish();
            }else{
                Toast.makeText(LoginActivity.this, "登录失败", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

        sp=getSharedPreferences("login",MODE_PRIVATE);
        readSP();
    }
    //读取sp信息
    private void readSP() {
        String strname=sp.getString("name",null);
        String strpass=sp.getString("pass",null);
        Boolean remember=sp.getBoolean("remember",false);

        if(remember)
        {
            etuser.setText(strname);
            etpass.setText(strpass);
        }

        sw.setChecked(remember);
    }
    //保存sp信息
    private void saveSP(){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("name",etuser.getText().toString());
        editor.putString("pass",etpass.getText().toString());
        editor.putBoolean("remember",sw.isChecked());
        editor.putString("sessionID",sessionID);
        editor.commit();
    }

    private void init() {
        findViewById(R.id.btnlogin).setOnClickListener(this);
        findViewById(R.id.btnregister).setOnClickListener(this);
        etuser = findViewById(R.id.editText);
        etpass = findViewById(R.id.editText2);
        sw=findViewById(R.id.switch1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnlogin:
                username=etuser.getText().toString();
                password=etpass.getText().toString();
                LoginModel loginModel=new LoginModel();
                loginModel.getLoginResult(username,password,loginListener);
                break;
            case R.id.btnregister:
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }
}
