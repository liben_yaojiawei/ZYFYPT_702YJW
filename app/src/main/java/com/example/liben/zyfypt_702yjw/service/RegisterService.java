package com.example.liben.zyfypt_702yjw.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RegisterService {
    @GET("api.php/reg")
    Call<String> reg(@Query("username") String username,
                     @Query("password") String password,
                     @Query("tel") String tel
                    );

}
