package com.example.liben.zyfypt_702yjw.model;

import com.example.liben.zyfypt_702yjw.bean.WareBean;
import com.example.liben.zyfypt_702yjw.common.Common;

import com.example.liben.zyfypt_702yjw.iface.WareIface;
import com.example.liben.zyfypt_702yjw.iface.WareListener;

import com.example.liben.zyfypt_702yjw.service.WareService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WareModel implements WareIface{
    private Retrofit retrofit;
    //构造函数
    public WareModel(){
        //使用Retrofit----1
        retrofit=new Retrofit.Builder()
                .baseUrl(Common.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public void getResultList(String mod, int page, String sessionID,final WareListener listener) {
        //使用Retrofit----2
        WareService service
                =retrofit.create(WareService.class);
        Call<List<WareBean>> call
                =service.getWareList(mod,page,sessionID);
        //使用Retrofit----3
        call.enqueue(new Callback<List<WareBean>>() {
            @Override
            public void onResponse(Call<List<WareBean>> call, Response<List<WareBean>> response) {
                if(response.isSuccessful() && response!=null)
                {  listener.onResponse(response.body());
                }
                else   listener.onFail("on response fail");
            }
            @Override
            public void onFailure(Call<List<WareBean>> call, Throwable t) {
                listener.onFail(t.toString());
            }
        });
    }


}

