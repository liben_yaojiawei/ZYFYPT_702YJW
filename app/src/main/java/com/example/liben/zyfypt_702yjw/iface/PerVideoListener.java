package com.example.liben.zyfypt_702yjw.iface;

import com.example.liben.zyfypt_702yjw.bean.PerVideoBean;

import java.util.List;

//获取网络数据结果
public interface PerVideoListener {
    //成功返回登录信息
    void onResponse(List<PerVideoBean> beanlist);
    //失败返回错误字符串
    void onFail(String msg);

}
