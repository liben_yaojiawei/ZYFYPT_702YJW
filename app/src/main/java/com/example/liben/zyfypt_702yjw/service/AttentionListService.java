package com.example.liben.zyfypt_702yjw.service;

import com.example.liben.zyfypt_702yjw.bean.ArticleBean;
import com.example.liben.zyfypt_702yjw.bean.FocusResult;
import com.example.liben.zyfypt_702yjw.bean.ProjectBean;
import com.example.liben.zyfypt_702yjw.bean.TcaseBean;
import com.example.liben.zyfypt_702yjw.bean.TwareBean;
import com.example.liben.zyfypt_702yjw.bean.UserBean;
import com.example.liben.zyfypt_702yjw.bean.VideoBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AttentionListService {
    @GET("api.php/lists/mod/{mod}")
    Call<List<ArticleBean>> getArticleList(@Path("mod") String mod, @Query("page") int page, @Header("SessionID") String sessionID, @Query("userid") String userid);

    @GET("api.php/lists/mod/{mod}")
    Call<List<ProjectBean>> getProjectList(@Path("mod") String mod, @Query("page") int page, @Header("SessionID") String sessionID, @Query("userid") String userid);

    @GET("api.php/lists/mod/{mod}")
    Call<List<TcaseBean>> getTcaseList(@Path("mod") String mod, @Query("page") int page, @Header("SessionID") String sessionID, @Query("userid") String userid);

    @GET("api.php/lists/mod/{mod}")
    Call<List<TwareBean>> getTwareList(@Path("mod") String mod, @Query("page") int page, @Header("SessionID") String sessionID, @Query("userid") String userid);

    @GET("api.php/lists/mod/{mod}")
    Call<List<VideoBean>> getVideoList(@Path("mod") String mod, @Query("page") int page, @Header("SessionID") String sessionID, @Query("userid") String userid);

    @GET("api.php/listmyfocus/mod/{mod}focus")
    Call<List<FocusResult<UserBean>>> getFocusUserList(@Path("mod") String mod, @Query("page") int page, @Header("SessionID") String sessionID);

}
