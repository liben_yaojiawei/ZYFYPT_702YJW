package com.example.liben.zyfypt_702yjw.iface;

import java.util.List;

public interface AttentListListener<T> {
    void onResponse(List<T> beanlist);
    void onFail(String msg);
}


