package com.example.liben.zyfypt_702yjw.iface;

public interface AttentionListener {
    void onResponse(String msg);
    void onFail(String msg);

}
