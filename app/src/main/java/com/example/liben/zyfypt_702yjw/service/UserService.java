package com.example.liben.zyfypt_702yjw.service;

import com.example.liben.zyfypt_702yjw.bean.LoginBean;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserService {
    @GET("api.php/login")
    Call<LoginBean> login(
            @Query("username") String username,
            @Query("password") String password
    );

}
