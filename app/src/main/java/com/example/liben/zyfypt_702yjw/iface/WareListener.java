package com.example.liben.zyfypt_702yjw.iface;


import com.example.liben.zyfypt_702yjw.bean.WareBean;

import java.util.List;

public interface WareListener {
    //成功返回登录信息
    void onResponse(List<WareBean> beanlist);
    //失败返回错误字符串
    void onFail(String msg);

}
