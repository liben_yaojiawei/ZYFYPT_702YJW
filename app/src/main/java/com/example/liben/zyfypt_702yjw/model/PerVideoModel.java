package com.example.liben.zyfypt_702yjw.model;

import com.example.liben.zyfypt_702yjw.bean.PerVideoBean;
import com.example.liben.zyfypt_702yjw.common.Common;
import com.example.liben.zyfypt_702yjw.iface.PerVideoListener;
import com.example.liben.zyfypt_702yjw.iface.PerVideoiface;
import com.example.liben.zyfypt_702yjw.service.PerVideoService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//获取视频数据
public class PerVideoModel implements PerVideoiface{
    private Retrofit retrofit;
    //构造函数
    public PerVideoModel(){
        //使用Retrofit----1
        retrofit=new Retrofit.Builder()
                .baseUrl(Common.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    @Override
    public void getResultList(String mod, int page, String sessionID, final PerVideoListener listener) {
        //使用Retrofit----2
        PerVideoService service
                =retrofit.create(PerVideoService.class);
        Call<List<PerVideoBean>> call
                =service.getPerVideoList(mod,page,sessionID);
        //使用Retrofit----3
        call.enqueue(new Callback<List<PerVideoBean>>() {
            @Override
            public void onResponse(Call<List<PerVideoBean>> call, Response<List<PerVideoBean>> response) {
                if(response.isSuccessful() && response!=null)
                {  listener.onResponse(response.body());
                }
                else   listener.onFail("on response fail");
            }
            @Override
            public void onFailure(Call<List<PerVideoBean>> call, Throwable t) {
                listener.onFail(t.toString());
            }
        });

    }
}
