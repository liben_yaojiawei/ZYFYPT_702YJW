package com.example.liben.zyfypt_702yjw.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.liben.zyfypt_702yjw.R;
import com.example.liben.zyfypt_702yjw.iface.RegisterListener;
import com.example.liben.zyfypt_702yjw.model.RegisterModel;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText username,password,tell;
    private Button   zhuce,fanhui;
    private String   name=" ",pass=" ",tellnumber=" ";

    private RegisterListener registerListener=new RegisterListener() {
        @Override
        public void onResponse(String n) {
            if(n!=null){
                Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
                finish();
            }else {
                Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        username=findViewById(R.id.editText3);
        password=findViewById(R.id.editText7);
        tell=findViewById(R.id.editText6);
        zhuce=findViewById(R.id.button);
        fanhui=findViewById(R.id.button2);
        zhuce.setOnClickListener(this);
        fanhui.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                name=username.getText().toString();
                pass=password.getText().toString();
                tellnumber=tell.getText().toString();
                RegisterModel registerModel=new RegisterModel();
                registerModel.getRegisterResult(name,pass,tellnumber,registerListener);
                break;
            case R.id.button2:
                finish();
                break;
        }
    }
}
