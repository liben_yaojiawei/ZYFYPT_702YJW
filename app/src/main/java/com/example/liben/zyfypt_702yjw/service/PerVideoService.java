package com.example.liben.zyfypt_702yjw.service;

import com.example.liben.zyfypt_702yjw.bean.PerVideoBean;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PerVideoService {
    @GET("api.php/listspecial/mod/{mod}")
    Call<List<PerVideoBean>> getPerVideoList(
            @Path("mod") String mod,
            @Query("page") int page,
            @Header("SessionID") String sessionID
    );

}
