package com.example.liben.zyfypt_702yjw.iface;

public interface Loginiface {
    void getLoginResult(
            String username,
            String password,
            LoginListener loginListener
    );
}
