package com.example.liben.zyfypt_702yjw.iface;

import com.example.liben.zyfypt_702yjw.bean.LoginBean;
//获取登录网络数据结果
public interface LoginListener {
    //成功返回登陆信息
    void onResponse(LoginBean loginBean);
    //失败返回错误字符串
    void onFail(String msg);
}
