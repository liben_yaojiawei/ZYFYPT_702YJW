package com.example.liben.zyfypt_702yjw.iface;

import com.example.liben.zyfypt_702yjw.bean.CollectBean;

import java.util.List;

public interface CollectListListener<T> {
    void onResponse(List<CollectBean<T>> beanlist);
    void onFail(String msg);
}


