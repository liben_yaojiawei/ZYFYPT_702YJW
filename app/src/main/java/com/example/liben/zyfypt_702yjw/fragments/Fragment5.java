package com.example.liben.zyfypt_702yjw.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.liben.zyfypt_702yjw.R;
import com.example.liben.zyfypt_702yjw.activities.AttentActivity;
import com.example.liben.zyfypt_702yjw.activities.CollectListActivity;

public class Fragment5 extends BaseFragment implements View.OnClickListener{

    private Button collect ;
    private Button attention ;


    public Fragment5() {
    }


    @Nullable
    @Override //生命周期方法，创建View
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.fragment5,container,false);

    }


    @Override//生命周期方法，View创建完成
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.out.println("--f5--"+getSessionId());
        collect = getView().findViewById(R.id.button4);
        attention = getView().findViewById(R.id.button3) ;
        collect.setOnClickListener(this);
        attention.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button3:
                startActivity(new Intent(getActivity(), AttentActivity.class));
                break;
            case R.id.button4:
                startActivity(new Intent(getActivity(), CollectListActivity.class));
                break;
            default:
                break;
        }
    }
}
