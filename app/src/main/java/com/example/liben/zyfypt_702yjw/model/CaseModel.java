package com.example.liben.zyfypt_702yjw.model;

import com.example.liben.zyfypt_702yjw.bean.CaseBean;
import com.example.liben.zyfypt_702yjw.common.Common;
import com.example.liben.zyfypt_702yjw.iface.CaseListener;
import com.example.liben.zyfypt_702yjw.iface.Caseiface;
import com.example.liben.zyfypt_702yjw.service.CaseService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CaseModel implements Caseiface {
    private Retrofit retrofit;
    //构造函数
    public CaseModel(){

        //使用Retrofit----1
        retrofit=new Retrofit.Builder()
                .baseUrl(Common.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    @Override
    public void getResultList(String mod, int page, String sessionID, final CaseListener listener) {
        //使用Retrofit----2
        CaseService service
                =retrofit.create(CaseService.class);
        Call<List<CaseBean>> call
                =service.getCaseList(mod,page,sessionID);
        //使用Retrofit----3
        call.enqueue(new Callback<List<CaseBean>>() {
            @Override
            public void onResponse(Call<List<CaseBean>> call, Response<List<CaseBean>> response) {
                if(response.isSuccessful() && response!=null)
                {  listener.onResponse(response.body());
                }
                else   listener.onFail("on response fail");
            }
            @Override
            public void onFailure(Call<List<CaseBean>> call, Throwable t) {
                listener.onFail(t.toString());
            }
        });
    }
}



