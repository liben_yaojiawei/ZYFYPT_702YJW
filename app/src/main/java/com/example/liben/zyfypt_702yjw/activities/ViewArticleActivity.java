package com.example.liben.zyfypt_702yjw.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.liben.zyfypt_702yjw.R;
import com.example.liben.zyfypt_702yjw.common.Common;
import com.example.liben.zyfypt_702yjw.iface.AttentionListener;
import com.example.liben.zyfypt_702yjw.iface.CollectListener;
import com.example.liben.zyfypt_702yjw.model.AttentionModel;

import com.example.liben.zyfypt_702yjw.model.CollectModel;

public class ViewArticleActivity extends AppCompatActivity {

    private int resid;
    private int idolid;
    private WebView webView;

    Context context;

    private Boolean flagcollect=false;//收藏标志
    private Boolean flagfocus=false;//关注标志

    private CollectModel collectmodel;//收藏model
    private AttentionModel attentionModel;//关注model

    private SharedPreferences sp;//简单存储
    private String sessionID="";  //sessionid

    CollectListener listener=new CollectListener() {
        @SuppressLint("RestrictedApi")
        @Override
        public void onResponse(String msg) {
            //获取菜单视图
            ActionMenuItemView item=findViewById(R.id.menucollect);
            //根据mode中response返回的字符串区分返回结果
            switch (msg)
            {
                case "2": System.out.println("----收藏成功");
                    flagcollect=true;
                    item.setTitle("取消收藏");
                    break;
                case "1":System.out.println("----收藏失败");
                    break;
                case "4":System.out.println("----取消收藏成功");
                    flagcollect=false;
                    item.setTitle("收藏");
                    break;
                case "3":System.out.println("----取消收藏失败");
                    break;
                case "5":System.out.println("----已收藏");
                    flagcollect=true;
                    item.setTitle("取消收藏");
                    break;
                case "6":System.out.println("----未收藏");
                    flagcollect=false;
                    item.setTitle("收藏");
                    break;
                default:
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        public void onFail(String msg) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    };

    AttentionListener attentionListener=new AttentionListener() {
        @SuppressLint("RestrictedApi")
        @Override
        public void onResponse(String msg) {
            //获取菜单视图
            ActionMenuItemView item=findViewById(R.id.menufocus);
            //根据mode中response返回的字符串区分返回结果
            switch (msg)
            {
                case "2": System.out.println("----关注成功");
                    flagfocus=true;
                    item.setTitle("取消关注");
                    break;
                case "1":System.out.println("----关注失败");
                    break;
                case "4":System.out.println("----取消关注成功");
                    flagfocus=false;
                    item.setTitle("关注");
                    break;
                case "3":System.out.println("----取消关注失败");
                    break;
                case "5":System.out.println("----已关注");
                    flagfocus=true;
                    item.setTitle("取消关注");
                    break;
                case "6":System.out.println("----未关注");
                    flagfocus=false;
                    item.setTitle("关注");
                    break;
                default:
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        public void onFail(String msg) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_activity_wenzhang);

        context=ViewArticleActivity.this;

        System.out.println("----查看文章详情");

        resid  = getIntent().getIntExtra("resid",1);
        idolid = getIntent().getIntExtra("userid",1);

        sp=context.getSharedPreferences("login",MODE_PRIVATE);
        readSP();//读取sessionid


        webView = (WebView)findViewById(R.id.webview);
        webView.loadUrl(Common.ARTICLEURL+resid);

    }
    private void readSP() {
        sessionID=sp.getString("sessionID",null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menucollect, menu);//加载菜单布局  改
        collectmodel=new CollectModel();//实例化对象
        collectmodel.exist("article",resid,sessionID,listener);//判断是否收藏

        attentionModel=new AttentionModel();
        attentionModel.exist("userfocus",idolid,sessionID,attentionListener);//判断是否关注

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menucollect:
                if(flagcollect)//如果已收藏，则调用取消收藏
                {
                    System.out.println("----准备取消收藏");
                    collectmodel.uncollect("article",resid,sessionID,listener);
                }
                else//如果未收藏，则调用收藏
                {
                    System.out.println("----准备收藏");
                    collectmodel.collect("article",resid,sessionID,listener);
                }
                break;
            case R.id.menufocus:
                if(flagfocus)//如果已关注，则调用取消关注
                {
                    System.out.println("----准备取消关注");
                    attentionModel.unattetntion("userfocus",idolid,sessionID,attentionListener);
                }
                else//如果未关注，则调用关注
                {
                    System.out.println("----准备关注");
                    attentionModel.attetntion("userfocus",idolid,sessionID,attentionListener);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }




}
