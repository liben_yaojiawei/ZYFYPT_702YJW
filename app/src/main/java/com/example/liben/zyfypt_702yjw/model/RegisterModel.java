package com.example.liben.zyfypt_702yjw.model;

import android.widget.Toast;

import com.example.liben.zyfypt_702yjw.activities.RegisterActivity;
import com.example.liben.zyfypt_702yjw.bean.LoginBean;
import com.example.liben.zyfypt_702yjw.common.Common;
import com.example.liben.zyfypt_702yjw.iface.RegisterListener;
import com.example.liben.zyfypt_702yjw.service.RegisterService;
import com.example.liben.zyfypt_702yjw.service.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterModel {
    private Retrofit retrofit;

    public RegisterModel(){
        retrofit = new Retrofit.Builder()
                .baseUrl(Common.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public void getRegisterResult(String username,String password,String tell,final RegisterListener registerListener){
        RegisterService registerService=retrofit.create(RegisterService.class);
        Call call=registerService.reg(username,password,tell);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response.isSuccessful() && response.body()!=null){
                    registerListener.onResponse(response.body().toString());
                }else {
                    registerListener.onFail("registerListener Fail");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                    registerListener.onFail(t.toString());
            }
        });
    }
}
