package com.example.liben.zyfypt_702yjw.iface;

import com.example.liben.zyfypt_702yjw.bean.VideoBean;

import java.util.List;

//获取网络数据结果
public interface VideoListener {
    //成功返回登录信息
    void onResponse(List<VideoBean> beanlist);
    //失败返回错误字符串
    void onFail(String msg);

}
